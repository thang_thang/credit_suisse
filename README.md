<h5>Compile to jar file </h5>
  <p>./gradlew jar </p>
<h5> Execute the application </h5>
 <p> java -jar ./build/libs/Credit_Suisse-1.0-SNAPSHOT.jar </p>

<h5>Assumptions: </h5>
  <p>- Application will continue and allow user to retry when command is invalid</p>
  <p>- If user provides more parameters than required, application still continue and picks only what needed</p>
  