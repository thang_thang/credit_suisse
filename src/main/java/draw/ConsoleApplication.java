package draw;

import draw.painter.ConsolePainter;

public class ConsoleApplication {
    private final ConsolePainter consolePainter;

    public ConsoleApplication() {
        this.consolePainter = new ConsolePainter();
    }

    public void run() {
        consolePainter.start();
    }
}
