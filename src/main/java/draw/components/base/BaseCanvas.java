package draw.components.base;

import lombok.Getter;
import lombok.extern.log4j.Log4j2;

@Getter
@Log4j2
public abstract class BaseCanvas<V, P extends BasePoint<V>> implements Drawable<V> {
    int xSize;
    int ySize;
    P[][] array;

    @SuppressWarnings("")
    public BaseCanvas(int xSize, int ySize) {
        this.xSize = xSize;
        this.ySize = ySize;
        array = initCanvas(xSize, ySize);
        initData();
    }

    protected abstract P[][] initCanvas(int xSize, int ySize);

    private void initData() {
        for (int i = 0; i < xSize; i++) {
            for (int j = 0; j < ySize; j++) {
                setPointValue(i, j, defaultEmptyPoint());
            }
        }
    }

    protected abstract P defaultEmptyPoint();

    private void setPointValue(int i, int j, V v) {
        P p = initPoint();
        p.setValue(v);
        setPointValue(i, j, p);
    }

    private void setPointValue(int i, int j, P p) {
        getArray()[i][j] = p;
    }

    /**
     * Draw line
     *
     * @param x1
     * @param y1
     * @param x2
     * @param y2
     */
    public void createLine(int x1, int y1, int x2, int y2) {
        log.info("create line from ({},{}) to ({},{})", x1, y1, x2, y2);
        validateX(x1, x2);
        validateY(y1, y2);
        if (x1 == x2) {
            createHorizontalLine(x1, y1, y2);
        } else if (y1 == y2) {
            createVerticalLine(x1, x2, y1);
        } else {
            throw new IllegalArgumentException("Currently only horizontal or vertical lines are supported");
        }
    }

    /**
     * vertical line
     *
     * @param x1
     * @param x2
     * @param yIndex
     */
    private void createVerticalLine(int x1, int x2, int yIndex) {
        //update to java zero index
        x1--;
        x2--;
        yIndex--;
        int from = Math.min(x1, x2);
        int to = Math.max(x1, x2);
        log.info("Vertical line");
        for (int i = from; i <= to; i++) {
            array[i][yIndex] = initPoint();
        }
    }

    protected abstract P initPoint();


    protected P initPoint(V value) {
        P p = initPoint();
        p.setValue(value);
        return p;
    }


    /**
     * horizontal line
     *
     * @param xIndex
     * @param y1
     * @param y2
     */
    private void createHorizontalLine(int xIndex, int y1, int y2) {
        //update to java zero index
        xIndex--;
        y1--;
        y2--;
        log.info("Horizontal line");
        int from = Math.min(y1, y2);
        int to = Math.max(y1, y2);
        for (int i = from; i <= to; i++) {
            array[xIndex][i] = initPoint();
        }
    }

    private void validateX(int... xValues) {
        for (int x : xValues) {
            if (x <= 0 || x > xSize) {
                throw new IllegalArgumentException("Invalid x index:" + x);
            }
        }

    }

    private void validateY(int... yValues) {
        for (int y : yValues) {
            if (y <= 0 || y > ySize) {
                throw new IllegalArgumentException("Invalid y index:" + y);
            }
        }
    }

    /**
     * create Rectangle
     *
     * @param x1
     * @param y1
     * @param x2
     * @param y2
     */
    public void createRectangle(int x1, int y1, int x2, int y2) {
        validateX(x1, x2);
        validateY(y1, y2);
        if (x1 >= x2 || y1 >= y2) {
            throw new IllegalArgumentException("Invalid values, expect upper left corner and lower right corner");
        }
        createLine(x1, y1, x2, y1);
        createLine(x2, y1, x2, y2);
        createLine(x2, y2, x1, y2);
        createLine(x1, y2, x1, y1);
    }

    public void fillArea(int x, int y, V newValue) {
        validateX(x);
        validateY(y);
        x--;
        y--;
        V previousValue = getPoint(x, y).getValue();

        if (previousValue.equals(newValue)) {
            log.info("No change");
        } else {
            fillArea(x, y, newValue, previousValue);
        }

    }

    public P getPoint(int x, int y) {
        return getArray()[x][y];
    }

    private void fillArea(int x, int y, V newValue, V previousValue) {
        if (x < 0 || x >= xSize || y < 0 || y >= ySize) {
            return;
        }
        V current = getPoint(x, y).getValue();
        if (!current.equals(previousValue) || current.equals(newValue)) {
            return;
        } else {
            setPointValue(x, y, newValue);
            fillArea(x + 1, y, newValue, previousValue); //fill right
            fillArea(x - 1, y, newValue, previousValue); //fill left
            fillArea(x, y + 1, newValue, previousValue); //fill top
            fillArea(x, y - 1, newValue, previousValue); //fill bottom
        }
    }
}
