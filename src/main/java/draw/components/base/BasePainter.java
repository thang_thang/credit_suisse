package draw.components.base;

public abstract class BasePainter<V, K extends BaseCanvas<V, ? extends BasePoint<V>>> {
    private static final String PLEASE_INIT_CANVAS = "Please init canvas";
    private static final String HELP = "Command \t\tDescription\n" +
            "C w h           Should create a new canvas of width w and height h.\n" +
            "L x1 y1 x2 y2   Should create a new line from (x1,y1) to (x2,y2). Currently only\n" +
            "                horizontal or vertical lines are supported. Horizontal and vertical lines\n" +
            "                will be drawn using the 'x' character.\n" +
            "R x1 y1 x2 y2   Should create a new rectangle, whose upper left corner is (x1,y1) and\n" +
            "                lower right corner is (x2,y2). Horizontal and vertical lines will be drawn\n" +
            "                using the 'x' character.\n" +
            "B x y c         Should fill the entire area connected to (x,y) with \"colour\" c. The\n" +
            "                behavior of this is the same as that of the \"bucket fill\" tool in paint\n" +
            "                programs.\n" +
            "Q               Should quit the program.";
    private static final String LINE = "==================";
    private static final String WAITING_NEW_COMMAND = "Waiting new command...";

    protected K canvas;

    protected final CanvasDrawer<V> drawer;

    public BasePainter(CanvasDrawer<V> drawer) {
        this.drawer = drawer;
    }

    /**
     * return false if exit
     *
     * @return
     */
    public abstract boolean readCommand();

    private boolean ready() {
        if (canvas == null) {
            drawer.draw(PLEASE_INIT_CANVAS);
            return false;
        } else {
            return true;
        }
    }

    public void newCanVas(int x, int y) {
        initCanvas(x, y);
        refresh();
    }

    protected abstract void initCanvas(int x, int y);


    public void drawRectangle(int x1, int y1, int x2, int y2) {
        if (ready()) {
            canvas.createRectangle(x1, y1, x2, y2);
            refresh();
        }
    }

    public void drawLine(int x1, int y1, int x2, int y2) {
        if (ready()) {
            canvas.createLine(x1, y1, x2, y2);
            refresh();
        }
    }

    public void fillArea(int x, int y, V value) {
        if (ready()) {
            canvas.fillArea(x, y, value);
            refresh();
        }
    }

    protected void refresh() {
        drawer.draw(canvas);
        drawLine();
        drawer.draw(WAITING_NEW_COMMAND);
    }

    public void start() {
        drawer.draw(HELP);
        drawLine();
        drawer.draw(PLEASE_INIT_CANVAS);
        while (readCommand()) {
            drawLine();
        }
    }

    public void drawLine() {
        drawer.draw(LINE);
    }
}
