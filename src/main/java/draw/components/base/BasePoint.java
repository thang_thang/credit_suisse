package draw.components.base;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class BasePoint<V> {
    private V value;
}
