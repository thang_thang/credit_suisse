package draw.components.base;

import java.io.IOException;

public interface CanvasDrawer<V> {

    void draw(Drawable<V> canvas);

    void draw(Object object);
}
