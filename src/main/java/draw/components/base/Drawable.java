package draw.components.base;

public interface Drawable<V> {
    V getDrawValue();
}
