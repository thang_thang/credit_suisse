package draw.components.impl;

import draw.components.base.CanvasDrawer;
import draw.components.base.Drawable;

public class ConsoleDrawer implements CanvasDrawer<String> {

    @Override
    public void draw(Drawable<String> canvas) {
        try {
            draw(canvas.getDrawValue());
        } catch (Exception e) {
            draw(e.getMessage());
        }
    }

    @Override
    public void draw(Object object) {
        System.out.println(object);
    }
}
