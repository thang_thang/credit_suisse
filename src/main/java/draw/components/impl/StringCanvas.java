package draw.components.impl;

import draw.components.base.BaseCanvas;
import draw.components.base.BasePoint;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class StringCanvas extends BaseCanvas<String, StringPoint> {
    private final String xBorder;

    public StringCanvas(int xSize, int ySize) {
        super(xSize, ySize);
        xBorder = xBorder();
        log.info("Created canvas {} ,{}", xSize, ySize);
    }

    @Override
    protected StringPoint[][] initCanvas(int xSize, int ySize) {
        return new StringPoint[xSize][ySize];
    }

    @Override
    protected StringPoint defaultEmptyPoint() {
        return new StringPoint(" ");
    }

    @Override
    protected StringPoint initPoint() {
        return new StringPoint("x");
    }


    /**
     * draw value
     *
     * @return
     */
    @Override
    public String getDrawValue() {
        return new StringBuilder()
                .append(xBorder)
                .append("\n")
                .append(bodyToString())
                .append(xBorder).toString();
    }

    /**
     * convert body to string
     *
     * @return
     */
    private String bodyToString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < getYSize(); i++) {
            stringBuilder.append("|");
            for (int j = 0; j < getXSize(); j++) {
                BasePoint<String> point = getPoint(j, i);
                String v = point == null ? " " : point.getValue();
                stringBuilder.append(v);
            }
            stringBuilder.append("|").append("\n");
        }
        return stringBuilder.toString();
    }


    /**
     * create border string
     *
     * @return
     */
    private String xBorder() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < getXSize() + 2; i++) {
            sb.append("-");
        }
        return sb.toString();
    }
}
