package draw.components.impl;

import draw.components.base.BasePoint;

public class StringPoint extends BasePoint<String> {

    public StringPoint(String pointValue) {
        super(pointValue);
    }
}
