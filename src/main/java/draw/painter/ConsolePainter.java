package draw.painter;

import draw.components.base.BasePainter;
import draw.components.impl.ConsoleDrawer;
import draw.components.impl.StringCanvas;
import org.apache.logging.log4j.util.Strings;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ConsolePainter extends BasePainter<String, StringCanvas> {

    private static final String CLOSING_APPLICATION = "Closing Application";
    private static final Pattern NEW_CANVAS_PATTERN = Pattern.compile("C (\\d+) (\\d+)");
    private static final Pattern NEW_LINE_PATTERN = Pattern.compile("L (\\d+) (\\d+) (\\d+) (\\d+)");
    private static final Pattern NEW_RECTANGLE_PATTERN = Pattern.compile("R (\\d+) (\\d+) (\\d+) (\\d+)");
    private static final Pattern FILL_AREA_PATTERN = Pattern.compile("B (\\d+) (\\d+) (.)");
    private static final String INVALID_COMMAND_PLEASE_RETRY = "Invalid command, please retry";
    private static final String ERROR = "ERROR:";


    public ConsolePainter() {
        super(new ConsoleDrawer());
    }

    @Override
    public boolean readCommand() {
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(System.in));

        // Reading data using readLine
        try {
            String command = reader.readLine();
            handleCommand(command);
            return true;
        } catch (IllegalArgumentException e) {
            drawLine();
            drawer.draw(ERROR + e.getMessage());
            return true;
        } catch (IOException e) {
            drawLine();
            drawer.draw(ERROR + e.getMessage());
            drawer.draw(CLOSING_APPLICATION);
            return false;
        }
    }

    private void handleCommand(String command) {
        drawer.draw("Entered command: " + command);
        if (Strings.isBlank(command)) {
            drawInvalid();
            return;
        }
        switch (command.charAt(0)) {
            case 'C':
                newCanvasCommand(command);
                return;
            case 'L':
                newLineCommand(command);
                return;
            case 'R':
                rectangleCommand(command);
                return;
            case 'B':
                fillAreaCommand(command);
                return;
            case 'Q':
                drawer.draw(CLOSING_APPLICATION);
                System.exit(0);
                return;
            default:
                drawInvalid();
        }
    }

    private void fillAreaCommand(String command) {
        Matcher matcher = FILL_AREA_PATTERN.matcher(command);
        if (matcher.find()) {
            int x = Integer.parseInt(matcher.group(1));
            int y = Integer.parseInt(matcher.group(2));
            String value = matcher.group(3);
            fillArea(x, y, value);
        } else {
            drawInvalid();
        }
    }

    private void rectangleCommand(String command) {
        Matcher matcher = NEW_RECTANGLE_PATTERN.matcher(command);
        if (matcher.find()) {
            int x1 = Integer.parseInt(matcher.group(1));
            int y1 = Integer.parseInt(matcher.group(2));
            int x2 = Integer.parseInt(matcher.group(3));
            int y2 = Integer.parseInt(matcher.group(4));
            drawRectangle(x1, y1, x2, y2);
        } else {
            drawInvalid();
        }
    }

    private void newLineCommand(String command) {
        Matcher matcher = NEW_LINE_PATTERN.matcher(command);
        if (matcher.find()) {
            int x1 = Integer.parseInt(matcher.group(1));
            int y1 = Integer.parseInt(matcher.group(2));
            int x2 = Integer.parseInt(matcher.group(3));
            int y2 = Integer.parseInt(matcher.group(4));
            drawLine(x1, y1, x2, y2);
        } else {
            drawInvalid();
        }
    }

    private void newCanvasCommand(String command) {
        Matcher matcher = NEW_CANVAS_PATTERN.matcher(command);
        if (matcher.find()) {
            int x = Integer.parseInt(matcher.group(1));
            int y = Integer.parseInt(matcher.group(2));
            initCanvas(x, y);
        } else {
            drawInvalid();
        }
    }

    private void drawInvalid() {
        drawer.draw(INVALID_COMMAND_PLEASE_RETRY);
    }

    @Override
    protected void initCanvas(int x, int y) {
        this.canvas = new StringCanvas(x, y);
        refresh();
    }

}
