package unit;

import draw.components.impl.StringCanvas;
import draw.components.impl.StringPoint;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class UnitTests {


    @Test
    void canvasTest() {
        int xSize = 20;
        int ySize = 4;
        StringCanvas canvas = new StringCanvas(xSize, ySize);
        assertEquals(xSize, canvas.getXSize());
        assertEquals(ySize, canvas.getYSize());
        StringPoint[][] array = canvas.getArray();
        assertNotNull(array);
        assertEquals(xSize, array.length);
        assertEquals(ySize, array[0].length);
        System.out.println(canvas.getDrawValue());

        String sampleOutput1 =
                "----------------------\n" +
                        "|                    |\n" +
                        "|                    |\n" +
                        "|                    |\n" +
                        "|                    |\n" +
                        "----------------------";
        System.out.println(canvas.getDrawValue());
        assertEquals(sampleOutput1, canvas.getDrawValue());

        //=====create Line

        //invalid x
        assertXIndexError(canvas, 0);
        assertXIndexError(canvas, -1);
        assertXIndexError(canvas, xSize + 1);
        //invalid y
        assertYIndexError(canvas, 0);
        assertYIndexError(canvas, -1);
        assertYIndexError(canvas, ySize + 1);

        //valid x,y
        canvas.createLine(1, 2, 6, 2);

        String sampleOutput2 =
                "----------------------\n" +
                        "|                    |\n" +
                        "|xxxxxx              |\n" +
                        "|                    |\n" +
                        "|                    |\n" +
                        "----------------------";
        System.out.println(canvas.getDrawValue());
        assertEquals(sampleOutput2, canvas.getDrawValue());

        canvas.createLine(6, 3, 6, 4);
        String sampleOutput3 =
                "----------------------\n" +
                        "|                    |\n" +
                        "|xxxxxx              |\n" +
                        "|     x              |\n" +
                        "|     x              |\n" +
                        "----------------------";
        System.out.println(canvas.getDrawValue());
        assertEquals(sampleOutput3, canvas.getDrawValue());


        //create rectangle
        //invalid x
        assertThrows(IllegalArgumentException.class, () -> canvas.createRectangle(13, 1, 13, 3));
        assertThrows(IllegalArgumentException.class, () -> canvas.createRectangle(14, 1, 13, 3));
        //invalid y
        assertThrows(IllegalArgumentException.class, () -> canvas.createRectangle(14, 3, 18, 3));
        assertThrows(IllegalArgumentException.class, () -> canvas.createRectangle(14, 4, 18, 3));

        canvas.createRectangle(14, 1, 18, 3);

        String sampleOutput4 =
                "----------------------\n" +
                        "|             xxxxx  |\n" +
                        "|xxxxxx       x   x  |\n" +
                        "|     x       xxxxx  |\n" +
                        "|     x              |\n" +
                        "----------------------";
        System.out.println(canvas.getDrawValue());
        assertEquals(sampleOutput4, canvas.getDrawValue());

        //fill
        //invalid x
        assertThrows(IllegalArgumentException.class, () -> canvas.fillArea(30, 3, "o"));
        //invalid y
        assertThrows(IllegalArgumentException.class, () -> canvas.fillArea(10, 30, "o"));
        canvas.fillArea(10, 3, "o");

        String sampleOutput5 =
                "----------------------\n" +
                        "|oooooooooooooxxxxxoo|\n" +
                        "|xxxxxxooooooox   xoo|\n" +
                        "|     xoooooooxxxxxoo|\n" +
                        "|     xoooooooooooooo|\n" +
                        "----------------------";
        System.out.println(canvas.getDrawValue());
        assertEquals(sampleOutput5, canvas.getDrawValue());

        canvas.fillArea(2, 3, "*");

        String sampleOutput6 =
                "----------------------\n" +
                        "|oooooooooooooxxxxxoo|\n" +
                        "|xxxxxxooooooox   xoo|\n" +
                        "|*****xoooooooxxxxxoo|\n" +
                        "|*****xoooooooooooooo|\n" +
                        "----------------------";
        System.out.println(canvas.getDrawValue());
        assertEquals(sampleOutput6, canvas.getDrawValue());
    }


    private void assertYIndexError(StringCanvas canvas, int y) {
        assertThrows(IllegalArgumentException.class, () -> canvas.createLine(1, y, 6, 2));
    }

    private void assertXIndexError(StringCanvas canvas, int x) {
        assertThrows(IllegalArgumentException.class, () -> canvas.createLine(x, 2, 6, 2));
    }

    @Test
    void testSquareCanvas() {
        StringCanvas canvas = new StringCanvas(3, 3);
        System.out.println(canvas.getDrawValue());

        String sampleOutput =
                "-----\n" +
                        "|   |\n" +
                        "|   |\n" +
                        "|   |\n" +
                        "-----";
        System.out.println(canvas.getDrawValue());
        assertEquals(sampleOutput,canvas.getDrawValue());
    }
}
